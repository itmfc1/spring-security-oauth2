package com.yjxxt;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityOauth2PasswordApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityOauth2PasswordApplication.class,args);
    }
}
