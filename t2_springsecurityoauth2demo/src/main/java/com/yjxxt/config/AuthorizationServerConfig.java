package com.yjxxt.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;


/**
 * 授权服务器配置
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                // 配置客户端id(开发者账号id)
                .withClient("admin")
                // 配置客户端密码(开发中id 对应密码)
                .secret(passwordEncoder.encode("112233"))
                //配置访问token的有效期
                //.accessTokenValiditySeconds(3600)
                // 重定向uri,用于授权成功后跳转
                .redirectUris("http://www.baidu.com")
                // 授权范围
                .scopes("all")
                // 授权类型 授权码模式
                .authorizedGrantTypes("authorization_code");
    }
}
