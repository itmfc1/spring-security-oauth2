package com.example.t1_spring_security_demo.service;


import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author fachang.mao
 * @Date 2021/11/21 15:47
 * @Version 1.0
 */
public interface MyService {

    boolean hasPermission(HttpServletRequest request, Authentication authentication);

}
