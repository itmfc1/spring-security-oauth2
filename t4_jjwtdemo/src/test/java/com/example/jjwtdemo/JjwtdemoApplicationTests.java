package com.example.jjwtdemo;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.Base64Codec;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
class JjwtdemoApplicationTests {

    /**
     * 创建token
     * https://jwt.io/
     */
    @Test
    public void testCreateToken() {
        //创建JwtBuilder对象
        JwtBuilder jwtBuilder = Jwts.builder()
                //声明的标识{"jti": "8888"}
                .setId("8888")
                //主体，用户{"sub" : "Rose"}
                .setSubject("Rose")
                //创建日期{"ita" : "xxxxxx"}
                .setIssuedAt(new Date())
                //算法 + 盐
                .signWith(SignatureAlgorithm.HS256, "salt_xxx");
        //获取jwt的token
        String token = jwtBuilder.compact();
        System.out.println(token);

        System.out.println("============");

        String[] split = token.split("\\.");
        System.out.println(Base64Codec.BASE64.decodeToString(split[0]));//标头：算法和令牌类型
        System.out.println(Base64Codec.BASE64.decodeToString(split[1]));//有效载荷：数据
        System.out.println(Base64Codec.BASE64.decodeToString(split[2]));//验证签名       --> 无法解密

    }

    /**
     * 解析token
     */
    @Test
    public void testParseToken(){
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODg4Iiwic3ViIjoiUm9zZSIsImlhdCI6MTYzNzUwOTExMX0.2A0r_9xGblGH9ftlH2UeIbCMhpPbtDVIPhNxt9qNkrg";
        //解析token获取负载中声明的对象
        Claims claims = Jwts.parser()
                .setSigningKey("salt_xxx")
                .parseClaimsJws(token)
                .getBody();

        System.out.println("Id:"+claims.getId());
        System.out.println("Subject:"+claims.getSubject());
        System.out.println("IssuedAt:"+claims.getIssuedAt());

    }


    /**
     * 创建token（失效时间）
     * https://jwt.io/
     */
    @Test
    public void testCreateTokenExp() {
        //当前系统时间
        long now = System.currentTimeMillis();
        //过期时间 1min
        long exp = now + 60 * 1000;
        //创建JwtBuilder对象
        JwtBuilder jwtBuilder = Jwts.builder()
                //声明的标识{"jti": "8888"}
                .setId("8888")
                //主体，用户{"sub" : "Rose"}
                .setSubject("Rose")
                //创建日期{"ita" : "xxxxxx"}
                .setIssuedAt(new Date())
                //算法 + 盐
                .signWith(SignatureAlgorithm.HS256, "salt_xxx")
                .setExpiration(new Date(exp));
        //获取jwt的token
        String token = jwtBuilder.compact();
        System.out.println(token);

        System.out.println("============");

        String[] split = token.split("\\.");
        System.out.println(Base64Codec.BASE64.decodeToString(split[0]));//标头：算法和令牌类型
        System.out.println(Base64Codec.BASE64.decodeToString(split[1]));//有效载荷：数据
        System.out.println(Base64Codec.BASE64.decodeToString(split[2]));//验证签名       --> 无法解密

    }

    /**
     * 解析token（失效时间）
     */
    @Test
    public void testParseTokenExp(){
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODg4Iiwic3ViIjoiUm9zZSIsImlhdCI6MTYzNzUxMDA2OSwiZXhwIjoxNjM3NTEwMTI5fQ.tzRmMrpiPQt09QeM_N3ONF9V8RklRdQiPzBmAA7V8y4";
        //解析token获取负载中声明的对象
        Claims claims = Jwts.parser()
                .setSigningKey("salt_xxx")
                .parseClaimsJws(token)
                .getBody();

        System.out.println("Id:"+claims.getId());
        System.out.println("Subject:"+claims.getSubject());
        System.out.println("IssuedAt:"+claims.getIssuedAt());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        System.out.println("签发时间:" + simpleDateFormat.format(claims.getIssuedAt()));
        System.out.println("过期时间:" + simpleDateFormat.format(claims.getExpiration()));
        System.out.println("当前时间:" + simpleDateFormat.format(new Date()));

    }


    /**
     * 创建token(自定义声明)
     * https://jwt.io/
     */
    @Test
    public void testCreateTokenClaims() {
        //创建JwtBuilder对象
        JwtBuilder jwtBuilder = Jwts.builder()
                //声明的标识{"jti": "8888"}
                .setId("8888")
                //主体，用户{"sub" : "Rose"}
                .setSubject("Rose")
                //创建日期{"ita" : "xxxxxx"}
                .setIssuedAt(new Date())
                //算法 + 盐
                .signWith(SignatureAlgorithm.HS256, "salt_xxx")
                //自定义声明
                //直接传入map
                //.addClaims(map)
                .claim("roles", "admin")
                .claim("logo", "xxx.jpg");
        //获取jwt的token
        String token = jwtBuilder.compact();
        System.out.println(token);

        System.out.println("============");

        String[] split = token.split("\\.");
        System.out.println(Base64Codec.BASE64.decodeToString(split[0]));//标头：算法和令牌类型
        System.out.println(Base64Codec.BASE64.decodeToString(split[1]));//有效载荷：数据
        System.out.println(Base64Codec.BASE64.decodeToString(split[2]));//验证签名       --> 无法解密

    }

    /**
     * 解析token(自定义声明)
     */
    @Test
    public void testParseTokenClaims(){
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODg4Iiwic3ViIjoiUm9zZSIsImlhdCI6MTYzNzU5MjA3NSwicm9sZXMiOiJhZG1pbiIsImxvZ28iOiJ4eHguanBnIn0.UaBP97l5FXDxqevihbtIozuCvvIYVeb7GivSpNmyiTs";
        //解析token获取负载中声明的对象
        Claims claims = Jwts.parser()
                .setSigningKey("salt_xxx")
                .parseClaimsJws(token)
                .getBody();

        System.out.println("Id:"+claims.getId());
        System.out.println("Subject:"+claims.getSubject());
        System.out.println("IssuedAt:"+claims.getIssuedAt());
        System.out.println("roles:"+claims.get("roles"));
        System.out.println("logo:"+claims.get("logo"));

    }

}
