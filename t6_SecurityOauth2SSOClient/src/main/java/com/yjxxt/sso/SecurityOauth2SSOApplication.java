package com.yjxxt.sso;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;

@SpringBootApplication
//开启单点登录功能
@EnableOAuth2Sso
public class SecurityOauth2SSOApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityOauth2SSOApplication.class,args);
    }
}
