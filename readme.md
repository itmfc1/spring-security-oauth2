# SpringSecurityOauth2 

> 根据BiliBili视频整理 ,原地址
>
> B站讲的最好的SpringSecurity+jwt+oauth2.0教程。7小时带你从入门到精通Spring Security实战-完整版教程_哔哩哔哩_bilibili
>
> https://www.bilibili.com/video/BV1gb4y1b7XE?from=search&seid=9909733692150927284&spm_id_from=333.337.0.0



## 说明

所有代码已经过本人运行，仅做学习交流所用



## 开发环境

```
JDK >= 1.8 (推荐1.8版本)
Mysql >= 5.7.0 (推荐5.7版本)
Redis >= 3.0
Maven >= 3.0
```



## 视频地址



B站讲的最好的SpringSecurity+jwt+oauth2.0教程。7小时带你从入门到精通Spring Security实战-完整版教程_哔哩哔哩_bilibili

https://www.bilibili.com/video/BV1gb4y1b7XE?from=search&seid=9909733692150927284&spm_id_from=333.337.0.0

```
P1 001_SpringSecurity学习目标 02:16
P2 002_SpringSecurity简介 06:19
P3 003_SpringSecurity快速Demo 06:10
P4 004_UserDetailsService详解 07:18
P5 005_PasswordEncoder详解 06:03
P6 006_自定义登录逻辑 07:20
P7 007_自定义登录页面 14:16
P8 008_失败跳转 04:56
P9 009_设置请求账户和密码的参数名 05:48
P10 010_自定义登录成功处理器 08:54
P11 011_自定义登录失败处理器 04:44
P12 012_anyRequest详解 03:02
P13 013_antMatchers详解 06:57
P14 014_regexMathcers详解 04:50
P15 015_mvcMatchers详解 04:23
P16 016_内置访问控制方法介绍 04:35
P17 017_权限判断 06:35
P18 018_角色判断 04:37
P19 019_IP地址判断 05:30
P20 020_自定义403处理方案 06:04
P21 021_access方法使用 03:24
P22 022_access结合自定义方法实现权限控制 06:57
P23 023_serured注解 07:04
P24 024_preAuthorize注解+postAuthorize注解 05:44
P25 025_RememberMe功能 14:58
P26 026_在thymeleaf中获取属性值 08:23
P27 027_在thymeleaf中进行权限判断 03:31
P28 028_退出登录 04:39
P29 029_退出登录源码解读 07:27
P30 030_SpringSecurity中的csrf 09:52
P31 031_Oauth2协议简介 15:41
P32 032_授权模式 08:38
P33 033_SpringSecurityOauth2架构 05:54
P34 034_SpringSecurityOauth2授权码模式 17:37
P35 035_授权码模式演示 13:27
P36 036_密码模式演示 05:49
P37 037_Redis存储Token 04:24
P38 038_常见认证机制 10:38
P39 039_什么是JWT 12:25
P40 040_JJWT快速Demo 11:07
P41 041_解析Token 04:01
P42 042_token过期校验 06:22
P43 043_自定义申明 04:17
P44 044_SpringSecurityOauth2整合JWT 07:31
P45 045_扩展JWT中存储的内容 09:12
P46 046_解析JWT中内容 05:47
P47 047_RefreshToken 04:01
P48 048_SpringSecurityOauth2整合SSO 12:37
```

